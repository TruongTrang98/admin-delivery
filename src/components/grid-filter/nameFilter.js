import React, { Component } from 'react'

export default class NameFilter extends Component {
  constructor (props) {
    super(props)
    this.state = {
      text: ''
    }
    this.input = React.createRef()
    this.valueGetter = this.props.valueGetter
    this.onChange = this.onChange.bind(this)
  }

  onChange (event) {
    let newValue = event.target.value
    if (this.state.text !== newValue) {
      this.setState({
        text: newValue
      }, () => {
        this.props.filterChangedCallback()
      })
    }
  }

  getModel () {
    return { value: this.state.text }
  }

  setModel (model) {
    this.state.text = model ? model.value : ''
  }

  isFilterActive () {
    return this.state.text !== null && this.state.text !== undefined && this.state.text !== ''
  }

  doesFilterPass (params) {
    return this.state.text.toLowerCase()
      .split(' ')
      .every((filterWord) => this.valueGetter(params.node).toString().toLowerCase().indexOf(filterWord) >= 0)
  }

  afterGuiAttached (params) {
    this.focus()
  }

  focus () {
    console.log('abc')
    window.setTimeout(() => {
      let container = this.input.current
      if (container) {
        container.focus()
      }
    })
  }

  render () {
    let style = {
      width: '150px',
      height: '25px',
      marginTop: '10px',
      maginRight: 'auto',
      marginLeft: 'auto'
    }

    return (
      <div style={style}>
        <input
          placeholder='Search'
          style={{ height: '20px', borderRadius: '5px' }} 
          ref={this.input} 
          value={this.state.text}
          onChange={this.onChange} 
          className='form-control'
        />
      </div>
    )
  }
}