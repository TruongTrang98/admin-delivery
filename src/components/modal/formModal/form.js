import React, { useState } from 'react'
import { Input, Button, Form, Radio } from 'antd'
import ImageUploader from 'react-images-upload'
import './index.less'

const hasErrors = (fieldsError, fieldsValue) => (
  Object.keys(fieldsError).some(field => fieldsError[field])
  || Object.values(fieldsValue).some(field => !field)
)

function MyForm (props) {
  const { getFieldDecorator, getFieldsValue, resetFields, getFieldsError } = props.form

  const [image, setImage] = useState('')
  const [type, setType] = useState('')
  const [loading, setLoading] = useState(false)

  const getBase64 = (file) => new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = () => resolve(reader.result)
    reader.onerror = error => reject(error)
  })

  const onDrop = async (picture) => {
    if (picture && picture[0]) {
      let imageBase64 = await getBase64(picture[0])
      setType(picture[0].type)
      setImage(imageBase64)
    } else {
      setImage('')
      setType('')
    }
  }

  const handleSubmit = async () => {
    setLoading(true)
    try {
      console.log(getFieldsValue())
      const input = { ...getFieldsValue(), password: '', image: { data: '', type: '' } }
      if (props.update) {
        await props.onSubmit(input, props.data._id)
      } else {
        // console.log('abc', { data: image, type })
        await props.onSubmit({ ...getFieldsValue(), image: { data: image, type } })
      }
      setImage('')
      setType('')
      setLoading(false)
      props.onCancel()
      resetFields()
    } catch (error) {
      setLoading(false)
      setImage('')
      setType('')
      resetFields()
      console.error(error)
    } 
  }

  const formItems = [
    {
      visible: true,
      field: 'name',
      type: 'text',
      label: `Tên ${props.type}`,
      getFieldDecoratorOpts: {
        initialValue: props.update && !!props.data ? props.data.name : '',
        rules: [
          { required: true, message: `Vui lòng nhập tên ${props.type}` },
          {
            pattern: /^[^\s]/,
            message: 'Không được có dấu cách đầu dòng'
          }
        ]
      },
      propsInput: {
        type: 'text',
        placeholder: `Tên ${props.type}`
      }
    },
    {
      visible: props.type === 'user' || props.type === 'shipper',
      field: 'gender',
      type: 'radio',
      label: 'Giới tính',
      getFieldDecoratorOpts: {
        initialValue: props.update && !!props.data ? props.data.gender : 'Khác',
        rules: [{ required: true, message: 'Vui lòng chọn giới tính' }]
      },
      propsInput: {}
    },
    {
      visible: true,
      field: 'email',
      type: 'text',
      label: 'Email',
      getFieldDecoratorOpts: {
        initialValue: props.update && !!props.data ? props.data.email : '',
        rules: [
          { required: true, message: 'Vui lòng nhập địa chỉ email' },
          {
            type: 'email', message: 'Địa chỉ email không hợp lệ'
          }
        ]
      },
      propsInput: {
        placeholder: 'Email',
        disabled: props.update
      }
    },
    {
      visible: true,
      field: 'phoneNumber',
      type: 'text',
      label: 'Số điện thoại',
      getFieldDecoratorOpts: {
        initialValue: props.update && !!props.data ? props.data.phoneNumber : '',
        rules: [
          { required: true, message: 'Vui lòng nhập số điện thoại' },
          {
            pattern: /((09|03|07|08|05)+([0-9]{8})\b)/g,
            message: 'Số điện thoại không hợp lệ'
          }
        ]
      },
      propsInput: {
        placeholder: 'Số điện thoại'
      }
    },
    {
      visible: props.type === 'shipper',
      field: 'cmnd',
      type: 'text',
      label: 'Chứng minh nhân dân',
      getFieldDecoratorOpts: {
        initialValue: props.update && !!props.data ? props.data.cmnd : '',
        rules: [
          { required: true, message: 'Vui lòng CMND' },
          {
            pattern: /^[0-9]{9}$/,
            message: 'CMND không hợp lệ'
          }
        ]
      },
      propsInput: {
        placeholder: 'Số điện thoại',
        disbled: true
      }
    },
    {
      visible: true,
      field: 'address',
      type: 'text',
      label: 'Địa chỉ',
      getFieldDecoratorOpts: {
        initialValue: props.update && !!props.data ? props.data.address : '',
        rules: [
          { required: true, message: 'Vui lòng nhập địa chỉ' }
        ]
      },
      propsInput: {
        placeholder: 'Địa chỉ'
      }
    },
    {
      visible: !props.update,
      field: 'password',
      type: 'password',
      label: 'Password',
      getFieldDecoratorOpts: {
        initialValue: '',
        rules: [
          { required: true, message: 'Vui lòng nhập mật khẩu' },
          { 
            pattern: /^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{8,}$/, 
            message: 'Password phải có ít nhất 8 kí tự bao gồm số và chữ cái và không có kí tự đặc biệt'
          }
        ]
      },
      propsInput: {
        type: 'password',
        placeholder: 'Password'
      }
    }
  ].map((item, idx) => (
    item.visible 
    && (
      <Form.Item key={`${idx}`} label={item.label}>
        {getFieldDecorator(item.field, {
          ...item.getFieldDecoratorOpts
        })(
          item.type === 'text'
            ? <Input {...item.propsInput} />
            : item.type === 'radio'
              ? (
                <Radio.Group>
                  <Radio value='Nam'>Nam</Radio>
                  <Radio value='Nữ'>Nữ</Radio>
                  <Radio value='Khác'>Khác</Radio>
                </Radio.Group>
              )
              : item.type === 'password'
                ? <Input.Password {...item.propsInput} />
                : null
        )}
      </Form.Item>
    )
  ))

  return (
    <>
      <Form>
        {formItems}
        {!props.update && (
          <Form.Item label='Avatar'>
            <ImageUploader
              key={props.imageUploadKey}
              withIcon
              singleImage
              withPreview
              buttonText='Chọn file'
              onChange={onDrop}
              buttonClassName={image ? 'disabledUpload' : ''}
              imgExtension={['.jpg', '.gif', '.png', '.gif']}
              label={'Chọn ảnh từ máy tính của bạn hoặc kéo thả vào đây'}
              labelStyles={{
                width: 200,
                textAlign: 'center'
              }}
              maxFileSize={5242880}
              buttonStyles={{
                color: '#fff',
                borderRadius: 3,
                backgroundColor: '#4ABFA9',
                borderColor: '#4ABFA9',
                textShadow: '0 -1px 0 rgba(0, 0, 0, 0.12)',
                boxShadow: '0 2px 0 rgba(0, 0, 0, 0.045)',
                lineHeight: 1.499,
                position: 'relative',
                display: 'inline-block',
                fontWeight: 400,
                whiteSpace: 'nowrap',
                textAlign: 'center',
                backgroundImage: 'none',
                border: '1px solid transparent'
              }}
            />
          </Form.Item>
        )}
      </Form>
      <div style={{ marginTop: 20 }}>
        <Button
          style={{ float: 'right' }}
          type='primary'
          loading={loading}
          onClick={handleSubmit}
          disabled={hasErrors(getFieldsError(), getFieldsValue()) || (image === '' && !props.update)}
        >
          Lưu
        </Button>
      </div>
      <div style={{ height: 20 }} />
    </>
  )
}

export default Form.create()(MyForm)