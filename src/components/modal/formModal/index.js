import React, { Component, useState } from 'react'
import { Modal, Button } from 'antd'
import MyForm from './form'

class FormModal extends Component {
  constructor (props) {
    super(props)
    this.state = {
      visible: false,
      imageUploadKey: 0
    }
  }

  componentDidMount () {
    this.props.getRef(this)
  }

  handleCancel = async () => {
    this.setState(prev => ({ visible: false, imageUploadKey: prev.imageUploadKey + 1 }))
  }

  handleSubmit = async (input, _id = null) => {
    if (!!this.props.update) {
      await this.props.onSubmit(_id, input)
    } else {
      await this.props.onSubmit(input)
    }
    
    this.setState(prev => ({ imageUploadKey: prev.imageUploadKey + 1 }))
  }

  showModal = () => {
    this.setState({ visible: true })
  }

  render () {
    return (
      <div style={{ marginBottom: '15px' }}>
        <Modal
          title={this.props.title}
          visible={this.state.visible}
          onCancel={this.handleCancel}
          footer={null}
        >
          <MyForm data={this.props.data} imageUploadKey={this.state.imageUploadKey} type={this.props.type} update={!!this.props.update} onSubmit={this.handleSubmit} onCancel={this.handleCancel} />
        </Modal>
      </div>
    )
  }
}

export default FormModal