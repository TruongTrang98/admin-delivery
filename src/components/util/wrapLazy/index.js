import React, { lazy } from 'react'
import { ClipLoader } from 'react-spinners'

import './index.less'

const Loading = () => (
  <div className='loadingLazy'>
    <ClipLoader sizeUnit='px' loading size={35} color='#4ABFA9' />
  </div>
)


const WrapLazy = importComponent => delay => lazy(
  () => new Promise(resolve => {
    setTimeout(() => {
      importComponent.then(Component => resolve(Component))
    }, delay)
  })
)

export { Loading, WrapLazy }
