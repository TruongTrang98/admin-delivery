/* eslint-disable */
import { observer, inject } from 'mobx-react'
import { withRouter } from 'react-router-dom'

const HOCMobX = Component => flagWithRouter => {
  if (!!flagWithRouter) return inject(store => store.store)(observer(withRouter(Component)))
  else return inject(store => store.store)(observer(Component))
}

export { HOCMobX }
