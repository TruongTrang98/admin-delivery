import React, { useState } from 'react'
import { Input, Select, Row, Col, Form } from 'antd'

import './index.less'

const { Option } = Select
const { Search } = Input

function SearchBar (props) {
  const [state, setState] = useState({
    txtSearch: '',
    txtFilter: 'all',
    isLoading: false
  })

  const handleChangeSearchText = (value) => {
    setState({
      ...state,
      txtSearch: value
    })
  }

  const handleChangeFilterText = async (value) => {
    setState({
      ...state,
      txtFilter: value
    })
    await props.onSearch(state.txtSearch, value)
  }

  const onSearch = async () => {
    setState({
      ...state,
      isLoading: true
    })
    await props.onSearch(state.txtSearch, state.txtFilter)
    setState({
      ...state,
      isLoading: false
    })
  }

  return (
    <Row>
      <Col span={10}>
        <Search 
          name='txtSearch' 
          placeholder='Nhập tên để tìm kiếm' 
          style={{ float: 'left' }} 
          onChange={({ target }) => handleChangeSearchText(target.value)}
          onPressEnter={onSearch}
        />
      </Col>
      <Col offset={1} span={4}>
        <Select onChange={(value) => handleChangeFilterText(value)} style={{ width: '100%' }} defaultValue='all'>
          <Option value='all'>Tất cả</Option>
          <Option value='isUnlocked'>Đang hoạt động</Option>
          <Option value='isLocked'>Tài khoản bị khóa</Option>
        </Select>
      </Col>
    </Row>
  )
}

export default SearchBar