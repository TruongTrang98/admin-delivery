import React, { Component } from 'react'
import { Modal, Input, Form, Button } from 'antd'


class ReasonDialog extends Component {
  constructor (props) {
    super(props)
    this.state = {
      loading: false,
      visible: false
    }
  }

  componentDidMount () {
    this.props.getRef(this)
  }

  handleLock = async () => {
    const { getFieldValue, resetFields } = this.props.form
    this.setState({ loading: true })
    await this.props.onLock(getFieldValue('reason'))
    this.setState({ loading: false, visible: false })
    resetFields()
  }

  render () {
    const { getFieldDecorator } = this.props.form
    return (
      <Modal
        title='Khóa tài khoản'
        footer={null}
        visible={this.state.visible}
        onCancel={() => this.setState({ visible: false })}
      >
        <Form>
          <Form.Item>
            {getFieldDecorator('reason', {
              rules: [{ required: true, message: 'Vui lòng nhập tên shop!' }]
            })(
              <Input.TextArea rows={4} placeholder='Nhập lí do khóa...' />
            )}
          </Form.Item>
          <div style={{ height: 30 }}>
            <Button
              style={{ float: 'right' }}
              type='primary'
              onClick={this.handleLock}
              loading={this.state.loading}
            >
              Khóa tài khoản
            </Button>
          </div>
        </Form>
      </Modal>
    )
  }
}

export default Form.create()(ReasonDialog)