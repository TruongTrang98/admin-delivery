import React, { useState } from 'react'
import { Avatar, Col, Row, Tag, Button } from 'antd'
import { Camera } from 'react-feather'
import moment from 'moment'
import { LOCK_AND_UNLOCK, 
  UPDATE_SHOP, UPDATE_USER, 
  UPDATE_SHIPPER, 
  UPDATE_SHIPPER_IMAGE, 
  UPDATE_SHOP_IMAGE, 
  UPDATE_USER_IMAGE 
} from './_query'
import { HOCQueryMutation, Notify } from '../../components/util'
import AvaterDialog from './AvatarDialog'
import ReasonDialog from './ReasonDialog'
import FormModal from '../../components/modal/formModal'

let refAvatar = null
let refReason = null
let refModal = null

function Content ({ data, refetch, mutate }) {
  const [loading, setLoading] = useState(false)
  const [hashImage, setHashImage] = useState(Date.now())
  
  const getRefAvatar = (value) => {
    refAvatar = value
  }

  const getRefReason = (value) => {
    refReason = value
  }

  const getRefModal = (value) => {
    refModal = value
  }

  async function handleUpdateImage (image) {
    try {
      let res = null
      let resStatus = null
      switch (data.type) {
        case 'user':
          res = await mutate.updateUserImage({
            variables: {
              _id: data._id,
              image
            }
          })
          resStatus = res.data.updateUserAvatar
          break
        case 'shipper': 
          res = await mutate.updateShipperImage({
            variables: {
              _id: data._id,
              image
            }
          })
          resStatus = res.data.updateShipperAvatar
          break
        case 'shop': 
          res = await mutate.updateShopImage({
            variables: {
              _id: data._id,
              image
            }
          })
          resStatus = res.data.updateShopAvatar
          break
        default: break
      }
      if (resStatus === '200') {
        setHashImage(Date.now())
        await refetch()
        return new Notify('success', 'Cập nhật ảnh thành công')
      } else {
        return new Notify('error', 'Cập nhật ảnh thất bại')
      }
    } catch (error) {
      console.error(error)
      return new Notify('error', 'Cập nhật ảnh thất bại')
    }
  }

  async function handleLockAndUnlock (reason = '') {
    try {
      setLoading(true)
      const res = await mutate.lockAndUnlock({
        variables: {
          _id: data.account._id,
          reason
        }
      })
      if (res.data.lockAndUnlockAccount === '200') {
        await refetch()
        setLoading(false)
        return new Notify('success', 'Thao tác thành công')
      } else {
        setLoading(false)
        return new Notify('success', 'Thao tác thành công')
      }
    } catch (error) {
      console.error(error)
      setLoading(false)
      return new Notify('error', 'Thao tác thất bại')
    }
  }

  const handleUpdateInfo = async (_id, input) => {
    console.log(_id, input)
    try {
      let res = null
      if (data.type === 'shop') {
        res = await mutate.updateShop({
          variables: {
            _id,
            input
          }
        })
      } else if (data.type === 'user') {
        res = await mutate.updateUser({
          variables: {
            _id,
            input
          }
        })
      } else if (data.type === 'shipper') {
        res = await mutate.updateShipper({
          variables: {
            _id,
            input
          }
        })
      }
      await refetch()
      return new Notify('success', 'Cập nhật thành công')
    } catch (error) {
      console.error(error)
      return new Notify('error', 'Cập nhật thất bại')
    } 
  }

  const dataInfo = [
    { name: 'Email', value: data.email, visible: true },
    { name: 'Địa chỉ', value: data.address, visible: true },
    { name: 'SĐT', value: data.phoneNumber, visible: true },
    { name: 'Giới tính', value: data.gender, visible: data.type === 'user' || data.type === 'shipper' },
    { name: 'CMND', value: data.cmnd, visible: data.type === 'shipper' },
    { name: 'Ngày tạo', value: moment(+data.createdAt).format('DD/MM/YYYY'), visible: true },
    { name: 'Cập nhật cuối', value: moment(+data.updatedAt).format('DD/MM/YYYY'), visible: true },
    { name: 'Lí do khóa', value: !!data.account.reason ? data.account.reason : 'Tài khoản không bị khóa', active: true, visible: true }
  ]
  const showData = dataInfo.map((prop, index) => (
    prop.visible && (
      <Row key={index.toString()} style={{ padding: 12 }}>
        <Col span={8}>
          <span style={{ fontWeight: 'bold' }}>{prop.name}</span>
        </Col>
        <Col span={16}>
          <span {...prop.active && { style: { color: 'red' } }}>{prop.value}</span>
        </Col>
      </Row>
    )
  ))

  const imagesLink = process.env.REACT_APP_imagesUrl

  return (
    <>
      <Row gutter={{ xs: 8, sm: 16, md: 24 }}>
        {/* avatar */}
        <Col span={4}>
          <div className='boxAvatar'>
            <Avatar 
              shape='square' 
              size='large'
              src={`${imagesLink}/${data.imageUrl}?${hashImage}`}
              style={{ cursor: 'pointer' }}
              onMouseEnter={() => {
                const imageIcon = document.getElementById('image_icon')
                if (imageIcon) {
                  document.getElementById('image_icon').style.color = 'white'
                }
              }}
              onMouseLeave={() => {
                const imageIcon = document.getElementById('image_icon')
                if (imageIcon) {
                  document.getElementById('image_icon').style.color = 'transparent'
                }
              }}
              onClick={() => refAvatar.setState({ visibleDialog: true })}
            />
            <Camera
              id='image_icon'
              style={{
                height: 35,
                width: 35,
                position: 'absolute',
                top: 61,
                left: 72,
                color: 'transparent',
                cursor: 'pointer'
              }}
              onMouseEnter={() => {
                const imageIcon = document.getElementById('image_icon')
                if (imageIcon) {
                  document.getElementById('image_icon').style.color = 'white'
                }
              }}
              onMouseLeave={() => {
                const imageIcon = document.getElementById('image_icon')
                if (imageIcon) {
                  document.getElementById('image_icon').style.color = 'transparent'
                }
              }}
              onClick={() => refAvatar.setState({ visibleDialog: true })}
            />
          </div>
          <AvaterDialog getRef={getRefAvatar} handleUpdate={handleUpdateImage} />
        </Col>
        {/* center content */}
        <Col span={9}>
          <Row className='row'>
            <div className='txtFullname'>{data.name}</div>
          </Row>
          <Row>
            <div className='txtID'>
              ID:
              &nbsp;
              <span>
                {data._id}
              </span>
            </div>
          </Row>
          <Row style={{ marginTop: '5px' }}>
            {data.account.isLocked ? <Tag color='red'>Tài khoản bị khóa</Tag> : <Tag color='green'>Đang hoạt động</Tag>}
          </Row>
          <Row style={{ marginTop: '10px' }}>
            <Button type='primary' style={{ marginRight: '15px' }} icon='edit' onClick={() => refModal.setState({ visible: true })}>Cập nhật</Button>
            {data.account.isLocked ? <Button loading={loading} icon='unlock' onClick={async () => { await handleLockAndUnlock() }}>Mở khóa</Button> : <Button onClick={() => refReason.setState({ visible: true })} icon='lock'>Khóa tài khoản</Button>}
            <ReasonDialog getRef={getRefReason} onLock={handleLockAndUnlock} />
            {/* <CreateShopModal refetch={refetch} isUpdate data={data} getRef={getRefModal} /> */}
            {data.type === 'shop' && (<FormModal getRef={getRefModal} title='Cập nhật shop' type='shop' update data={data} onSubmit={handleUpdateInfo} />)}
            {data.type === 'user' && (<FormModal getRef={getRefModal} title='Cập nhật user' type='user' update data={data} onSubmit={handleUpdateInfo} />)}
            {data.type === 'shipper' && (<FormModal getRef={getRefModal} title='Cập nhật shipper' type='shipper' update data={data} onSubmit={handleUpdateInfo} />)}
          </Row>
        </Col>
        {/* right content */}
        <Col span={11}>
          {showData}
        </Col>
      </Row>
    </>
  )
}

export default HOCQueryMutation([
  {
    mutation: UPDATE_USER_IMAGE,
    name: 'updateUserImage',
    options: {}
  },
  {
    mutation: UPDATE_SHOP_IMAGE,
    name: 'updateShopImage',
    options: {}
  },
  {
    mutation: UPDATE_SHIPPER_IMAGE,
    name: 'updateShipperImage',
    options: {}
  },
  {
    mutation: LOCK_AND_UNLOCK,
    name: 'lockAndUnlock',
    options: {}
  },
  {
    mutation: UPDATE_SHOP,
    name: 'updateShop'
  },
  {
    mutation: UPDATE_USER,
    name: 'updateUser'
  },
  {
    mutation: UPDATE_SHIPPER,
    name: 'updateShipper'
  }
])(Content)