import React from 'react'
import { withRouter } from 'react-router-dom'
import { Divider, Button, Avatar } from 'antd'


import Content from './Content'
import { HOCQueryMutation } from '../../components/util'
import { SHOP_BY_ID, USER_BY_ID, SHIPPER_BY_ID } from './_query'

import './index.less'

function Info (props) {
  const { data } = props
  let dataContent = null
  switch (props.match.params.type) {
    case 'shop':
      dataContent = { ...data.shopById, type: 'shop' }
      break
    case 'user':
      dataContent = { ...data.userById, type: 'user' }
      break
    case 'shipper':
      dataContent = { ...data.shipperById, type: 'shipper' }
      break
    default: 
  }
  return (
    <>
      <div className='Profile'>
        <div style={{ display: 'flex', justifyContent: 'flex-start' }}>
          <Button
            name='btn-back'
            type='primary'
            className='back-btn'
            onClick={() => props.history.goBack()}
            style={{
              marginTop: 19,
              padding: 0,
              height: 25,
              width: 18,
              marginRight: 10
            }}
          >
            <svg
              style={{ transform: 'translateX(-4px)' }}
              width='17'
              height='17'
              viewBox='0 0 18 17'
              fill='none'
              stroke='currentColor'
              strokeWidth='1.2'
              strokeLinecap='round'
              strokeLinejoin='round'
            >
              <polyline points='15 18 9 12 15 6' />
            </svg>
          </Button>
          <div className='TopContent'>
            <div className='Label'>
              Thông tin
            </div>
          </div>
        </div>
        <Divider type='horizontal' />
        <Content data={{ ...dataContent, type: props.match.params.type }} refetch={data.refetch} />
      </div>
    </>
  )
}

export default HOCQueryMutation([
  {
    query: SHOP_BY_ID,
    skip: (props) => props.match.params.type !== 'shop',
    options: (props) => ({
      variables: {
        _id: props.match.params.id
      },
      fetchPolicy: 'network-only'
    })
  },
  {
    query: USER_BY_ID,
    skip: (props) => props.match.params.type !== 'user',
    options: (props) => ({
      variables: {
        _id: props.match.params.id
      },
      fetchPolicy: 'network-only'
    })
  },
  {
    query: SHIPPER_BY_ID,
    skip: (props) => props.match.params.type !== 'shipper',
    options: (props) => ({
      variables: {
        _id: props.match.params.id
      },
      fetchPolicy: 'network-only'
    })
  }
])(withRouter(Info))