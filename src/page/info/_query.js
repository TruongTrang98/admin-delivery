import gql from 'graphql-tag'

export const SHOP_BY_ID = gql`
  query ($_id: String!){
    shopById(_id: $_id){
      _id
      name
      imageUrl
      address
      email
      phoneNumber
      isActive
      account {
        _id
        isLocked
        reason
      }
      createdAt
      updatedAt
    }
  }
`

export const USER_BY_ID = gql`
  query ($_id: String!){
    userById(_id: $_id){
      _id
      name
      gender
      address
      email
      phoneNumber
      imageUrl
      isActive
      account {
        _id
        isLocked
        reason
      }
      createdAt
      updatedAt
    }
  }
`

export const SHIPPER_BY_ID = gql`
  query ($_id: String!){
    shipperById(_id: $_id){
      _id
      name
      gender
      imageUrl
      address
      email
      phoneNumber
      cmnd
      isActive
      account {
        _id
        isLocked
        reason
      }
      createdAt
      updatedAt
    }
  }
`

export const UPDATE_USER_IMAGE = gql`
  mutation ($_id: String!, $image: ImageInput!){
    updateUserAvatar(_id: $_id, image: $image)
  }
`

export const UPDATE_SHOP_IMAGE = gql`
  mutation ($_id: String!, $image: ImageInput!){
    updateShopAvatar(_id: $_id, image: $image)
  }
`

export const UPDATE_SHIPPER_IMAGE = gql`
  mutation ($_id: String!, $image: ImageInput!){
    updateShipperAvatar(_id: $_id, image: $image)
  }
`

export const LOCK_AND_UNLOCK = gql`
  mutation ($_id: String!, $reason: String!){
    lockAndUnlockAccount(_id: $_id, reason: $reason)
  }
`

export const UPDATE_SHOP = gql`
  mutation ($_id: String!, $input: ShopInput!){
    updateShopInfo(_id: $_id, input: $input)
  }
`

export const UPDATE_USER = gql`
  mutation ($_id: String!, $input: UserInput!){
    updateUserInfo(_id: $_id, input: $input)
  }
`

export const UPDATE_SHIPPER = gql`
  mutation ($_id: String!, $input: ShipperInput!){
    updateShipperInfo(_id: $_id, input: $input)
  }
`