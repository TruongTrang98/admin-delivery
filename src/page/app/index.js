import { hot } from 'react-hot-loader/root'
import React from 'react'
import gql from 'graphql-tag'
import jwt from 'jsonwebtoken'
import {
  BrowserRouter,
  Switch,
  Route,
  HashRouter,
  Redirect
} from 'react-router-dom'
import { Provider, inject, observer } from 'mobx-react'
import { ApolloProvider, withApollo } from 'react-apollo'
import { flowRight as compose } from 'lodash'
import { Store, Client } from '../../tools'
import { LoadableComponent } from '../../components/util/loadable'
import { HOCQueryMutation } from '../../components/util/hocquerymutation'
// import './index.less'

@inject(store => store.store)
@observer
class Delivery extends React.Component {
  constructor (props) {
    super(props)
    this.state = { user: null, profile: null }
    this._getUserProfile = this.getUserProfile.bind(this)
  }

  componentWillMount () {
    this.getUserProfile()
  }

  componentWillReceiveProps (props) {
    if (props.isLogin && !this.props.isLogin) {
      this.getUserProfile()
    }
  }

  getUserProfile () {
    this.getUser()
    this.getProfile()
  }

  getUser () {
    let {
      mutate: { user },
      Authen: { onLogout }
    } = this.props
    try {
      let token = localStorage.getItem('TOKEN')
        ? jwt.verify(localStorage.getItem('TOKEN'), 'delivery')
        : { _id: '' }
      let userId = token._id
      user({
        variables: {
          _id: userId
        }
      })
        .then(({ data }) => {
          this.setState({ user: data.user })
        })
        .catch(() => {
          this.setState({ user: null })
          onLogout()
        })
    } catch (err) {
      console.log(err)
      this.setState({ user: null })
      onLogout()
    }
  }

  getProfile () {
    let {
      mutate: { profile },
      Authen: { getProfile, onLogout }
    } = this.props
    try {
      let token = localStorage.getItem('TOKEN')
        ? jwt.verify(localStorage.getItem('TOKEN'), 'delivery')
        : { _id: '' }
      let userId = token._id
      profile({
        variables: {
          _id: userId
        }
      })
        .then(({ data }) => {
          this.setState({ profile: null })
        })
        .catch(() => {
          this.setState({ profile: null })
          getProfile({})
          onLogout()
        })
    } catch (err) {
      console.log(err)
      this.setState({ profile: null })
      getProfile({})
      onLogout()
    }
  }

  render () {
    const { isLogin } = this.props.Authen
    return (
      <HashRouter>
        <BrowserRouter basename='/delivery'>
          <Switch>
            {isLogin ? (
              <Route
                path='/'
                component={LoadableComponent(import(`../router`), {
                  user: this.state.user,
                  profile: this.state.profile
                })}
              />
            ) : (
              <Route
                exact
                path='/login'
                component={LoadableComponent(import(`../login`), {
                  getUserProfile: this._getUserProfile
                })}
              />
            )}
            {isLogin ? <Redirect to='/' /> : <Redirect to='/login' />}
          </Switch>
        </BrowserRouter>
      </HashRouter>
    )
  }
}

const DeliveryData = HOCQueryMutation([
  {
    mutation: gql`
      mutation getProfile($_id: String) {
        user(_id: $_id) {
          _id
          name
        }
      }
    `,
    name: 'profile'
  },
  {
    mutation: gql`
      mutation user($_id: String) {
        user(_id: $_id) {
          _id
          name
        }
      }
    `,
    name: 'user'
  }
])(Delivery)

const store = new Store()

const App = () => (
  <Provider store={store}>
    <ApolloProvider client={Client}>
      <DeliveryData />
    </ApolloProvider>
  </Provider>
)

export default hot(App)
