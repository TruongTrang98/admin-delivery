import gql from 'graphql-tag'

export const USERS = gql`
  query users($txtSearch: String, $txtFilter: String, $offset: Int!, $limit: Int!) {
    users(txtSearch: $txtSearch, txtFilter: $txtFilter, offset: $offset, limit: $limit){
      _id
      name
      imageUrl
      gender
      email
      phoneNumber
      address
      account{
        _id
        username
        password
        roleId
        isLocked
        reason
        isActive
        createdAt
        updatedAt
      }
      isActive
      createdAt
      updatedAt
    },
    numberOfUser(txtSearch: $txtSearch, txtFilter: $txtFilter)
  }
`

export const CREATE_USER = gql`
  mutation ($input: UserInput!){
    createUser(input: $input)
  }
`