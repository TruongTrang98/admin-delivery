import React, { useState } from 'react'
import { Button, Pagination } from 'antd'

import Grid from './grid/Grid'
import SearchBar from '../../components/searchBar'
import { USERS, CREATE_USER } from './_query'
import { HOCQueryMutation, Notify } from '../../components/util'
import FormModal from '../../components/modal/formModal'

let refModal = null

function User (props) {
  localStorage.setItem('path', 'user')

  const [currentPage, setCurrentPage] = useState(1)

  const { data: { users, refetch, numberOfUser }, mutate: { createUser } } = props

  function getRefModal (value) {
    refModal = value
  }

  const handleCreate = async (input) => {
    try {
      const res = await createUser({
        variables: {
          input
        }
      })
      if (res.data.createUser === '200') {
        await refetch()
        return new Notify('success', 'Tạo user thành công')
      } else {
        return new Notify('error', 'Tạo user thất bại')
      }
    } catch (error) {
      console.error(error)
      return new Notify('error', 'Tạo user thất bại')
    } 
  }

  const onSearch = async (txtSearch, txtFilter) => {
    await refetch({ txtSearch, txtFilter })
  }

  const handleChangePage = (page) => {
    setCurrentPage(page)
    props.data.refetch({
      limit: 20,
      offset: (+page - 1) * 20
    })
  }

  // 20 là số row mỗi trang (limit = 20)
  const totalPage = numberOfUser % 20 > 0 ? Math.round((numberOfUser / 20) + 1) : Math.round(numberOfUser / 20)
  console.log(totalPage)


  return (
    <>
      <div style={{ width: '80%', height: '100%' }}>
        <SearchBar onSearch={onSearch} />
        <div style={{ marginBottom: '10px' }}>
          <FormModal getRef={getRefModal} title='Tạo user mới' type='user' onSubmit={handleCreate} />
          <Button type='primary' onClick={() => refModal.setState({ visible: true })} icon='plus'>
            Thêm user
          </Button>
        </div>
        <div style={{ width: '100%', height: '65%' }}>
          <Grid rowData={users} />
        </div>
        {numberOfUser > 0 && (
          <div style={{ marginTop: 20, display: 'flex', flexDirection: 'row', justifyContent: 'flex-end' }}>
            <Pagination current={currentPage} total={numberOfUser} pageSize={20} onChange={(page) => handleChangePage(page)} />
          </div>
        )}
      </div>
    </>
  )
}

export default HOCQueryMutation([
  {
    query: USERS,
    name: 'users',
    options: {
      variables: {
        txtSearch: '',
        txtFilter: '',
        offset: 0,
        limit: 20
      }
    }
  },
  {
    mutation: CREATE_USER,
    name: 'createUser'
  }
])(User)