import React, { useState } from 'react'
import { Input, Form, Icon, Button } from 'antd'
import gql from 'graphql-tag'
import * as jwt from 'jsonwebtoken'
import { Notify, HOCMobX, HOCQueryMutation } from '../../components/util'

import './index.css'

function Login (props) {
  const [isLoading, setIsLoading] = useState(false)
  const { getFieldDecorator, getFieldsError, setFieldsValue, setFields, validateFields, getFieldsValue } = props.form

  const handleLogin = ({ username, password }) => {
    setIsLoading(true)
    const {
      Authen: { onLogin },
      mutate: { login },
      getUserProfile
    } = props
    validateFields(errors => {
      if (errors) {
        console.log(errors)
      } else {
        login({
          variables: {
            input: {
              username,
              password
            }
          }
        })
          .then(({ data }) => {
            const token = jwt.verify(data.adminLogin.token, 'delivery')
            console.log(token)
            if (token.role === 'admin') {
              localStorage.setItem('username', username)
              onLogin(data.adminLogin.token)
              setIsLoading(false)
              getUserProfile()
              return new Notify('success', 'Đăng nhập thành công')
            } else {
              setIsLoading(false)
              setFields({
                username: {
                  errors: [new Error('Tên đăng nhập hoặc mật khẩu sai. Vui lòng nhập lại')]
                },
                password: {
                  errors: [new Error('Tên đăng nhập hoặc mật khẩu sai. Vui lòng nhập lại')]
                }
              })
              if (document.querySelector(`.clear-icon`)) {
                document.querySelector(`.clear-icon`).style.display = 'none'
              }
              return new Notify('error', 'Đăng nhập thất bại')
            }
          })
          .catch((err) => {
            setIsLoading(false)
            console.log(err)
            setFields({
              username: {
                errors: [new Error('Tên đăng nhập hoặc mật khẩu sai. Vui lòng nhập lại')]
              },
              password: {
                errors: [new Error('Tên đăng nhập hoặc mật khẩu sai. Vui lòng nhập lại')]
              }
            })
            if (document.querySelector(`.clear-icon`)) {
              document.querySelector(`.clear-icon`).style.display = 'none'
            }
            return new Notify('error', 'Đăng nhập thất bại')
          })
      }
    })
  }

  const handleEnter = e => {
    if (e.keyCode === 13) {
      e.preventDefault()
      handleLogin(getFieldsValue(['username', 'password']))
    }
  }

  return (
    <div className='container'>
      <div className='form-wrapper'>
        <div className='form-container'>
          <h1 style={{ textAlign: 'center' }}>
            <span style={{ fontSize: 65, color: '#4ABFA9', fontFamily: 'Arial' }}>A</span>
            <span>FoodNow</span>
          </h1>
          <Form className='login-form'>
            <Form.Item>
              {getFieldDecorator('username', {
                rules: [{ required: true, message: 'Please input your username!' }]
              })(
                <Input
                  onKeyDown={handleEnter}
                  prefix={<Icon type='user' style={{ color: 'rgba(0,0,0,.25)' }} />}
                  placeholder='Username'
                />,
              )}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator('password', {
                rules: [{ required: true, message: 'Please input your Password!' }]
              })(
                <Input.Password
                  onKeyDown={handleEnter}
                  prefix={<Icon type='lock' style={{ color: 'rgba(0,0,0,.25)' }} />}
                  type='password'
                  placeholder='Password'
                />,
              )}
            </Form.Item>
            <Form.Item>
              <Button loading={isLoading} type='primary' className='login-form-button' onClick={() => handleLogin(getFieldsValue(['username', 'password']))}>
                Log in
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  )
}

const LOGIN = gql`
  mutation login($input: LoginInput!) {
    adminLogin(input: $input) {
      token
      profile
    }
  }
`

export default HOCQueryMutation([{ mutation: LOGIN, name: 'login' }])(Form.create()(HOCMobX(Login)(true)))