import gql from 'graphql-tag'

export const SHIPPERS = gql`
  query($offset: Int!, $limit: Int!, $txtSearch: String, $txtFilter: String) {
    shippers(offset: $offset, limit: $limit, txtSearch: $txtSearch, txtFilter: $txtFilter){
      _id
      name
      imageUrl
      gender
      email
      phoneNumber
      address
      cmnd
      account{
        _id
        username
        password
        roleId
        isLocked
        reason
        isActive
        createdAt
        updatedAt
      }
      isActive
      createdAt
      updatedAt
    },
    numberOfShipper (txtSearch: $txtSearch, txtFilter: $txtFilter)
  }
`

export const CREATE_SHIPPER = gql`
  mutation ($input: ShipperInput!){
    createShipper(input: $input)
  }
`