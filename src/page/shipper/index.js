import React, { useState } from 'react'
import { Button, Pagination } from 'antd'
import Grid from './grid/Grid'

import SearchBar from '../../components/searchBar'
import { HOCQueryMutation, Notify } from '../../components/util'
import FormModal from '../../components/modal/formModal'
import { SHIPPERS, CREATE_SHIPPER } from './_query'

let refModal = null

function Shipper (props) {
  localStorage.setItem('path', 'shipper')

  const [currentPage, setCurrentPage] = useState(1)

  const { data: { shippers, refetch, numberOfShipper }, mutate: { createShipper } } = props

  function getRefModal (value) {
    refModal = value
  }

  const handleCreate = async (input) => {
    try {
      const res = await createShipper({
        variables: {
          input
        }
      })
      await refetch()
      return new Notify('success', 'Tạo shipper thành công')
    } catch (error) {
      console.error(error)
      return new Notify('error', 'Tạo shipper thất bại')
    } 
  }

  const handleChangePage = (page) => {
    setCurrentPage(page)
    props.data.refetch({
      limit: 20,
      offset: (+page - 1) * 20
    })
  }

  const onSearch = async (txtSearch, txtFilter) => {
    await refetch({ txtSearch, txtFilter })
  }

  return (
    <>
      <div style={{ width: '80%', height: '100%' }}>
        <SearchBar onSearch={onSearch} />
        <div style={{ marginBottom: '10px' }}>
          <FormModal getRef={getRefModal} title='Tạo shipper mới' type='shipper' onSubmit={handleCreate} />
          <Button type='primary' onClick={() => refModal.setState({ visible: true })} icon='plus'>
            Thêm shipper
          </Button>
        </div>
        <div style={{ width: '100%', height: '65%' }}>
          <Grid rowData={shippers} />
        </div>
        {numberOfShipper > 0 && (
          <div style={{ marginTop: 20, display: 'flex', flexDirection: 'row', justifyContent: 'flex-end' }}>
            <Pagination current={currentPage} total={numberOfShipper} pageSize={20} onChange={(page) => handleChangePage(page)} />
          </div>
        )}
      </div>
    </>
  )
}

export default HOCQueryMutation([
  {
    query: SHIPPERS,
    name: 'shippers',
    options: {
      variables: {
        offset: 0,
        limit: 20
      }
    }
  },
  {
    mutation: CREATE_SHIPPER,
    name: 'createShipper'
  }
])(Shipper)