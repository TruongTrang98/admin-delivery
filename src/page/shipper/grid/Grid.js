import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { Tag, Avatar, Empty } from 'antd'
import { AgGridReact } from 'ag-grid-react'
import { ChevronRight } from 'react-feather'
import 'ag-grid-community/dist/styles/ag-grid.css'
import 'ag-grid-community/dist/styles/ag-theme-balham.css'

import NameFilter from '../../../components/grid-filter/nameFilter'

import './grid.less'

class Grid extends Component {
  onGridReady = (params) => {
    params.api.sizeColumnsToFit()
  }

  onRowSelected = (lol) => {
    this.props.history.push(`/info/shipper/${lol.data._id}`)
  }

  render () {
    const imagesLink = process.env.REACT_APP_imagesUrl

    const cellStyles = {
      display: 'flex',
      'align-items': 'center',
      border: 'none',
      cursor: 'pointer'
    }

    const columnDefs = [
      {
        headerName: 'Hình ảnh',
        field: '',
        cellStyle: cellStyles,
        width: 100,
        cellRendererFramework: ({ data }) => (
          <Avatar 
            shape='square' 
            size='large'
            src={`${imagesLink}/${data.imageUrl}`}
            style={{ cursor: 'pointer' }}
          />
        )
      },
      {
        headerName: 'Tên shipper',
        field: 'name',
        cellStyle: cellStyles,
        filter: 'nameFilter',
        sortable: true
      },
      {
        headerName: 'Giới tính',
        field: 'gender',
        cellStyle: cellStyles,
        sortable: true
      },
      {
        headerName: 'CMND',
        field: 'cmnd',
        cellStyle: cellStyles,
        sortable: true
      },
      {
        headerName: 'Email',
        field: 'email',
        cellStyle: cellStyles,
        sortable: true
      },
      {
        headerName: 'Số điện thoại',
        field: 'phoneNumber',
        cellStyle: cellStyles,
        sortable: true
      },
      {
        headerName: 'Địa chỉ',
        field: 'address',
        cellStyle: cellStyles,
        sortable: true
      },
      {
        headerName: 'Hoạt động',
        field: 'active',
        cellStyle: cellStyles,
        cellRendererFramework: (lol) => (
          <>
            {lol.data.account.isLocked === false ? <Tag color='green'>Đang hoạt động</Tag> : <Tag color='red'>Tài khoản bị khóa</Tag>}
          </>
        )
      },
      { 
        headerName: '',
        field: '',
        maxWidth: '50',
        cellStyle: cellStyles,
        cellRendererFramework: () => (<ChevronRight style={{ paddingTop: '10' }} />)
      }
    ]
    
    return (
      <>
        {this.props.rowData <= 0 ? (
          <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', width: '100%', height: '100%' }}>
            <Empty description={<span>Không có shipper</span>} />
          </div>
        ) : (
          <div 
            className='ag-theme-balham'
            style={{ height: '100%' }} 
          >
            <AgGridReact
              columnDefs={columnDefs}
              rowData={this.props.rowData} 
              getRowHeight={() => 50}
              animateRows
              onGridReady={this.onGridReady}
              onRowClicked={this.onRowSelected}
              suppressHorizontalScroll
              frameworkComponents={{ nameFilter: NameFilter }}
            />
          </div>
        )}
      </>
    )
  }
}
export default withRouter(Grid)