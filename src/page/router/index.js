import React, { Component, Suspense } from 'react'
import { Switch, Redirect, Route, Link } from 'react-router-dom'
import { Menu, Icon, Layout, Dropdown, Input, PageHeader } from 'antd'
import { listPrivateRoute } from '../../config/router'
import ChangePasswordModal from '../../components/modal/changePasswordModal'
import { PrivateRoute, WrapLazy, Loading, HOCMobX, Notify } from '../../components/util'
import './index.less'

const { SubMenu } = Menu
const { Header, Sider, Content } = Layout

const languageMenu = (
  <Menu>
    <Menu.Item>
        Tiếng Việt
    </Menu.Item>
    <Menu.Item>
        English
    </Menu.Item>
  </Menu>
)

let refChangePassword = null

class Router extends Component {
  constructor (props) {
    super(props)
    this.state = {
      collapsed: false
    }
  }

  toggle = () => {
    this.setState(prevState => ({
      collapsed: !prevState.collapsed
    }))
  }

  handleLogout = () => {
    this.props.Authen.onLogout()
    return new Notify('success', 'Đăng xuất thành công')
  }

  getRefChangePassword = (value) => {
    refChangePassword = value
  }

  // handleShowModal = () => {
  //   if (refChangePassword) {
  //     refChangePassword.setModal(true)
  //   }
  // }
  iconclick = () => {
    if (this.props.location.pathname !== '/') {
      this.props.history.push('/')
    }
  }

  render () {
    return (
      <>
        <ChangePasswordModal getRef={this.getRefChangePassword} />
        <Layout style={{ height: window.innerHeight, zIndex: 0 }}>
          <Sider trigger={null} collapsible collapsed={this.state.collapsed} style={{ background: '#fff' }}>
            <div className='logo' onClick={this.iconclick}>
              {
                !this.state.collapsed ? <img src='https://graphqldelivery.herokuapp.com/images/logo.png' alt='logo' width='170px' /> : <img src='https://graphqldelivery.herokuapp.com/images/logo-rut-gon.png' alt='logo' width='50px' />
              }
            </div>
            <Menu theme='light' mode='inline' defaultSelectedKeys={[localStorage.getItem('path') ? localStorage.getItem('path') : 'user']}>
              {listPrivateRoute.map((route, idx) => (
                route.exact && (
                  <Menu.Item key={route.itemName}>
                    <Link to={route.path}>
                      <Icon type={route.icon} />
                      <span>{route.displayName}</span>
                    </Link>
                  </Menu.Item>
                )
              ))}
            </Menu>
          </Sider>
          <Layout>
            <PageHeader 
              title={(
                <Icon
                  className='trigger'
                  type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                  onClick={this.toggle}
                />
              )} 
              extra={[
                <Dropdown 
                  key='1' 
                  overlay={
                    (
                      <Menu>
                        <Menu.Item onClick={() => {
                          if (refChangePassword) {
                            refChangePassword.setState({ visible: true })
                          }
                        }}
                        >
                          Đổi mật khẩu
                        </Menu.Item>
                        <Menu.Item 
                          onClick={this.handleLogout}
                        >
                            Đăng xuất
                        </Menu.Item>
                      </Menu>
                    )
                  }
                >
                  <Icon type='user' className='user-icon' /> 
                </Dropdown>,
                <Dropdown key='2' overlay={languageMenu}>
                  <Icon type='global' className='global-icon' />
                </Dropdown>
              ]}
            />
            <Content
              style={{
                margin: '10px 16px',
                padding: 24,
                background: '#fff'
              }}
            >
              <Suspense fallback={<Loading />}>
                <Switch>
                  {listPrivateRoute.map((route, idx) => (
                    <PrivateRoute
                      key={idx}
                      exact={route.exact}
                      path={route.path}
                      component={WrapLazy(import(`../${route.component}`))(300)}
                      route={route}
                      profile={this.profile}
                    />
                  ))}
                  <Redirect to='/user' />
                  <Route component={WrapLazy(import(`../404`))(250)} />
                </Switch>
              </Suspense>
            </Content>
          </Layout>
        </Layout>
      </>
    )
  }
}

export default HOCMobX(Router)(true)
