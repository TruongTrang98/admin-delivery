import React from 'react'

const NotFound = () => (
  <div className='NotFound'>
    <div className='content'>404 Not Found !</div>
  </div>
)

export default NotFound