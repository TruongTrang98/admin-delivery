import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { Tag, Avatar, Empty } from 'antd'
import { ChevronRight } from 'react-feather'
import { AgGridReact } from 'ag-grid-react'
import 'ag-grid-community/dist/styles/ag-grid.css'
import 'ag-grid-community/dist/styles/ag-theme-balham.css'

import NameFilter from '../../../components/grid-filter/nameFilter'

import './grid.less'

class Grid extends Component {
  constructor (props) {
    super(props)
    this.state = { modal: null }
  }

  onGridReady = (params) => {
    params.api.sizeColumnsToFit()
  }

  onRowSelected = (lol) => {
    this.props.history.push(`/info/shop/${lol.data._id}`)
  }

  render () {
    const imagesLink = process.env.REACT_APP_imagesUrl

    const cellStyles = {
      display: 'flex',
      'align-items': 'center',
      border: 'none',
      cursor: 'pointer'
    }
    const columnDefs = [
      {
        headerName: 'Hình ảnh',
        field: '',
        cellStyle: cellStyles,
        width: 100,
        cellRendererFramework: ({ data }) => (
          <Avatar 
            shape='square' 
            size='large'
            src={`${imagesLink}/${data.imageUrl}`}
            style={{ cursor: 'pointer' }}
          />
        )
      },
      {
        headerName: 'Tên shop',
        field: 'name',
        cellStyle: cellStyles,
        filter: 'nameFilter',
        sortable: true
      },
      {
        headerName: 'Email',
        field: 'email',
        cellStyle: cellStyles,
        sortable: true
      },
      {
        headerName: 'Số điện thoại',
        field: 'phoneNumber',
        cellStyle: cellStyles,
        sortable: true
      },
      {
        headerName: 'Địa chỉ',
        field: 'address',
        cellStyle: cellStyles,
        sortable: true
      },
      {
        headerName: 'Tình trạng',
        field: 'account.isLocked',
        cellStyle: cellStyles,
        cellRendererFramework: ({ data }) => (
          <>
            {!data.account.isLocked ? <Tag style={{ cursor: 'pointer' }} color='green'>Đang hoạt động</Tag> : <Tag style={{ cursor: 'pointer' }} color='red'>Tài khoản bị khóa</Tag>}
          </>
        )
      },
      { 
        headerName: '',
        field: '',
        maxWidth: '50',
        cellStyle: cellStyles,
        cellRendererFramework: () => (<ChevronRight style={{ paddingTop: '10' }} />)
      }
    ]
    return (
      <>
        {this.props.rowData <= 0 ? (
          <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', width: '100%', height: '100%' }}>
            <Empty description={<span>Không có shop</span>} />
          </div>
        ) : (
          <div 
            className='ag-theme-balham'
            style={{ height: '100%' }} 
          >
            <AgGridReact
              columnDefs={columnDefs}
              rowData={this.props.rowData} 
              getRowHeight={() => 50}
              animateRows
              onGridReady={this.onGridReady}
              onRowClicked={this.onRowSelected}
              suppressHorizontalScroll
              // domLayout='autoHeight'
              frameworkComponents={{ nameFilter: NameFilter }}
            />
          </div>
        )}
      </>
    )
  }
}
export default withRouter(Grid)