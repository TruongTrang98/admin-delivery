import gql from 'graphql-tag'

export const SHOPS = gql`
  query shops ($offset: Int!, $limit: Int!, $txtSearch: String, $txtFilter: String) {
    shopsForAdmin (offset: $offset, limit: $limit, txtSearch: $txtSearch, txtFilter: $txtFilter){
      _id
      name
      imageUrl
      address
      phoneNumber
      email
      isActive
      account{
        _id
        username
        isLocked
        reason
      }
      createdAt
      updatedAt
    },
    numberOfShopForAdmin(txtSearch: $txtSearch, txtFilter: $txtFilter)
  }
`
export const CREATE_SHOP = gql`
  mutation ($input: ShopInput!){
    createShop(input: $input)
  }
`