import React, { Component, useState } from 'react'
import { Button, Pagination } from 'antd'

import SearchBar from '../../components/searchBar'
import Grid from './grid/Grid'
import FormModal from '../../components/modal/formModal'
import { SHOPS, CREATE_SHOP } from './_query'
import { HOCQueryMutation, Notify } from '../../components/util'

let refModal = null

function Shop (props) {
  const { data: { shopsForAdmin, refetch, numberOfShopForAdmin }, mutate: { createShop } } = props
  localStorage.setItem('path', 'shop')

  const [currentPage, setCurrentPage] = useState(1)
  
  function getRefModal (value) {
    refModal = value
  }

  const handleCreate = async (input) => {
    try {
      const res = await createShop({
        variables: {
          input
        }
      })
      await refetch()
      return new Notify('success', 'Tạo shop thành công')
    } catch (error) {
      console.error(error)
      return new Notify('error', 'Tạo shop thất bại')
    } 
  }

  const handleChangePage = (page) => {
    setCurrentPage(page)
    props.data.refetch({
      limit: 20,
      offset: (+page - 1) * 20
    })
  }

  const onSearch = async (txtSearch, txtFilter) => {
    await refetch({ txtSearch, txtFilter })
  }

  return (
    <>
      <div style={{ width: '80%', height: '100%' }}>
        <SearchBar onSearch={onSearch} />
        <div style={{ marginBottom: '10px' }}>
          {/* <CreateShopModal refetch={refetch} getRef={getRefModal} /> */}
          <FormModal getRef={getRefModal} title='Tạo shop mới' type='shop' onSubmit={handleCreate} />
          <Button type='primary' onClick={() => refModal.setState({ visible: true })} icon='plus'>
            Thêm shop
          </Button>
        </div>
        <div style={{ width: '100%', height: '65%' }}>
          <Grid rowData={shopsForAdmin} />
        </div>
        {numberOfShopForAdmin > 0 && (
          <div style={{ marginTop: 20, display: 'flex', flexDirection: 'row', justifyContent: 'flex-end' }}>
            <Pagination current={currentPage} total={numberOfShopForAdmin} pageSize={20} onChange={(page) => handleChangePage(page)} />
          </div>
        )}
      </div>
    </>
  )
}

export default HOCQueryMutation([
  {
    query: SHOPS,
    name: 'shops',
    options: () => ({
      variables: {
        offset: 0,
        limit: 20
      }
    })
  },
  {
    mutation: CREATE_SHOP,
    name: 'createShop'
  }
])(Shop)