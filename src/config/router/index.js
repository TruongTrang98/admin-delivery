const listPrivateRoute = [
  {
    displayName: 'Người dùng',
    path: '/user',
    component: 'user',
    exact: true,
    icon: 'user',
    itemName: 'user'
  },
  {
    displayName: 'Shop',
    path: '/shop',
    component: 'shop',
    exact: true,
    icon: 'shop',
    itemName: 'shop'
  },
  {
    displayName: 'Shipper',
    path: '/shipper',
    component: 'shipper',
    exact: true,
    icon: 'car',
    itemName: 'shipper'
  },
  {
    displayName: 'Thông tin',
    path: '/info/:type/:id',
    component: 'info',
    exact: false,
    icon: '',
    itemName: ''
  }
]
  
export { listPrivateRoute }