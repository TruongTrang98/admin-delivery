const {
  override,
  fixBabelImports,
  addLessLoader,
  useEslintRc,
  addDecoratorsLegacy,
  useBabelRc
} = require('customize-cra')

const WebpackBar = require('webpackbar')

const addProgressBarPlugin = () => config => {
  config.plugins.push(
    new WebpackBar()
  )
  return config
}

module.exports = override(
  fixBabelImports('import', {
    libraryName: 'antd',
    libraryDirectory: 'es',
    style: true
  }),
  addLessLoader({
    javascriptEnabled: true,
    modifyVars: { '@primary-color': '#4ABFA9' }
  }),
  useEslintRc(),
  useBabelRc(),
  addDecoratorsLegacy(),
  addProgressBarPlugin()
)
